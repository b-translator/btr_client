cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup of the important data

_EOF
}

cmd_backup() {
    set -x
    ds exec drush --yes @local_bcl vset maintenance_mode 1

    # clear the cache
    ds exec drush --yes @local_bcl cache-clear all

    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # copy the data to the backup dir
    ds inject backup.sh $backup

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/

    # enable the site
    ds exec drush --yes @local_bcl vset maintenance_mode 0
}
